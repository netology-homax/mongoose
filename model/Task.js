const mongoose = require('../storage');
const config = require('../config');

let {Schema} = mongoose;

let schema = new Schema({
  name: {
    type: String,
    required: true
  },
  status: {
      type: Boolean,
      required: true,
      default: false
  },
  level: {
    type: String,
    required: true
  },
  assigned: {
    type: Schema.Types.ObjectId
  }
});

module.exports = mongoose.model(config.mongodb.collections.tasks, schema);
