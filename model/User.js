const mongoose = require('../storage');
const config = require('../config');

let {Schema} = mongoose;

let schema = new Schema({
  name: {
    type: String,
    required: true
  },
  age: {
    type: Number
  },
  position: {
    type: String
  }
});

module.exports = mongoose.model(config.mongodb.collections.users, schema);
