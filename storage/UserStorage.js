const storage = require('./index');
const model = require('../model');
let {User} = model;

class UserStorage {
  find() {
    return new Promise((resolve, reject) => {
        User.find((err, users) => {
            if (err) {
              reject(err);
            }
            resolve(users);
        });
    })
  }

  findOne(id) {
    return new Promise((resolve, reject) => {
        User.findOne({_id: id}, (err, user) => {
            if (err) {
              reject(err);
            }
            resolve(user);
        });
    });
  }

  update(id, data) {
    return new Promise((resolve, reject) => {
        User.findOneAndUpdate({ _id: id }, data, {"new": true}, (err, updated) => {
          if (err) {
            reject(err);
          }
          resolve(updated);
        });
    });
  }

  remove(id) {
    return new Promise((resolve, reject) => {
        User.remove({ _id: id }, err => {
          if (err) {
            reject(err);
          }
          resolve();
        });
    });
  }

}

module.exports = new UserStorage();
