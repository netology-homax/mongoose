const storage = require('./index');
const model = require('../model');
let {Task} = model;

class TaskStorage {
  find() {
    return new Promise((resolve, reject) => {
        Task.find((err, tasks) => {
            if (err) {
              reject(err);
            }
            resolve(tasks);
        });
    })
  }

  search(data) {
    return new Promise((resolve, reject) => {
        Task
          .aggregate({$match: data})
          .exec((err, tasks) => {
              if (err) {
                reject(err);
              }
              resolve(tasks);
          });
    })
  }

  update(id, data) {
    return new Promise((resolve, reject) => {
        Task.findOneAndUpdate({ _id: id }, data, {"new": true}, (err, updated) => {
          if (err) {
            reject(err);
          }
          resolve(updated);
        });
    });
  }

  remove(id) {
    return new Promise((resolve, reject) => {
        Task.remove({ _id: id }, err => {
          if (err) {
            reject(err);
          }
          resolve();
        });
    });
  }

}

module.exports = new TaskStorage();
