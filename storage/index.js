const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const config = require('../config');

mongoose.connect(config.mongodb.connect);

module.exports = mongoose;
