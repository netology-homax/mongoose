const http = require('http');

class ExtendableError extends Error {
  constructor(message) {
    super(message);
    this.name = this.constructor.name;
    this.message = message;
    if (typeof Error.captureStackTrace === 'function') {
      Error.captureStackTrace(this, this.constructor);
    } else {
      this.stack = (new Error(message)).stack;
    }
  }
}

class HttpError extends ExtendableError {
  constructor(status, message) {
    let m = message || http.STATUS_CODES[status] || 'Error';
    super(m);
    this.status = status;
  }
}

exports.HttpError = HttpError;
