const express = require('express');
let app = module.exports = express();
const config = require('../../../config');

let {HttpError} = require('../../../error');
const models = require('../../../model');
const Storage = require('../../../storage/TaskStorage');
const UserStorage = require('../../../storage/UserStorage');
let {Task, User} = models;

app.get('/', function(req, res, next) {
  let {sort} = req.query;
  User
    .aggregate([
      {$lookup: {
        from: config.mongodb.collections.tasks,
        localField: '_id',
        foreignField: 'assigned',
        as: 'tasks'
      }},
      {$project: {
        name: 1,
        age: 1,
        position: 1,
        filtered_tasks: {
          $filter: {
            input: "$tasks",
            as: "task",
            cond: {$eq: ["$$task.status", true]}
          }
        }
      }},
      {$project: {
        name: 1,
        age: 1,
        position: 1,
        tasks: {
          $size: "$filtered_tasks"
        }
      }},
      {$sort: {"tasks": sort === 'asc' ? 1 : -1}}
    ])
    .exec((err, tasks) => {
        if (err) {
          console.log('errorchik', err);
          next(new Error('Couldn\'t find tasks'));
        }
        res.json(tasks);
    });
});
