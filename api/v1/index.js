const express = require('express');
let app = module.exports = express();

let apiV1Users = require('./users');
let apiV1Tasks = require('./tasks');
let apiV1Stats = require('./stats');

app.use('/users', apiV1Users);
app.use('/tasks', apiV1Tasks);
app.use('/stats', apiV1Stats);
