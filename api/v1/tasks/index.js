const express = require('express');
let app = module.exports = express();

let {HttpError} = require('../../../error');
const models = require('../../../model');
const Storage = require('../../../storage/TaskStorage');
const UserStorage = require('../../../storage/UserStorage');
let {Task, User} = models;

app.get('/', function(req, res, next) {
  let result = Storage.find()
    .then(tasks => {
      res.json(tasks);
    })
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t get tasks'));
    });
});

app.get('/search', function(req, res, next) {
  let {name, status, level, assigned} = req.query;
  let search = {};
  if (name) {
    search.name = {$regex: name};
  }
  if (status) {
    search.status = {$regex: status};
  }
  if (level) {
    search.level = {$regex: level};
  }
  if (assigned) {
    search.assigned = {$regex: assigned};
  }
  let result = Storage.search(search)
    .then(tasks => {
      res.json(tasks);
    })
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t find tasks'));
    });
});

app.post('/', function(req, res, next) {
  let name = req.body.name;
  let level = req.body.level;
  let task = new Task({name, level});
  task.save((err, task, affected) => {
      if (err) {
        console.log('errorchik', error);
        next(new Error('Couldn\'t add new task'));
        return;
      }
      res.json(task);
  });
});

app.post('/:id/assign/:user_id', function(req, res, next) {
  let id = req.params.id;
  let user_id = req.params.user_id;
  UserStorage.findOne(user_id)
    .then(user => {
      if (!user) {
        throw new HttpError(404, 'User not found');
        return;
      }
      return Storage.update(id, {assigned: user._id});
    })
    .then(task => {
      if (!task) {
          throw new HttpError(404, 'Task not found');
          return;
      }
      res.json(task);
    })
    .catch(err => {
      if (err instanceof HttpError) {
        next(err);
        return;
      }
      console.log('errorchik', err);
      next(new Error('Couldn\'t assign task to user'));
    })
});

app.put('/:id', function(req, res, next) {
  let id = req.params.id;
  let name = req.body.name;
  let level = req.body.level;
  let status = req.body.status;
  let data = {};
  if (name) {
    data.name = name;
  }
  if (level) {
    data.level = level;
  }
  if (status) {
    data.status = status;
  }
  Storage.update(id, data)
    .then(task => res.json(task))
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t update a task'));
    });
});

app.delete('/:id', function(req, res, next) {
  let id = req.params.id;
  Storage.remove(id)
    .then(() => res.send('ok'))
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t delete a task'));
    });
});
