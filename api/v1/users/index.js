const express = require('express');
let app = module.exports = express();

const models = require('../../../model');
const Storage = require('../../../storage/UserStorage');
let {User} = models;

app.get('/', function(req, res, next) {
  let result = Storage.find()
    .then(users => {
      res.json(users);
    })
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t get users'));
    });
});

app.post('/', function(req, res, next) {
  let name = req.body.name;
  let age = req.body.age;
  let position = req.body.position;
  let user = new User({name, age, position});
  user.save((err, user, affected) => {
      if (err) {
        console.log('errorchik', error);
        next(new Error('Couldn\'t add new contact'));
        return;
      }
      res.json(user);
  });
});

app.put('/:id', function(req, res, next) {
  let id = req.params.id;
  let name = req.body.name;
  let age = req.body.age;
  let position = req.body.position;
  let data = {};
  if (name) {
    data.name = name;
  }
  if (age) {
    data.age = age;
  }
  if (position) {
    data.position = position;
  }
  Storage.update(id, data)
    .then(user => res.json(user))
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t update a user'));
    });
});

app.delete('/:id', function(req, res, next) {
  let id = req.params.id;
  Storage.remove(id)
    .then(() => res.send('ok'))
    .catch(error => {
      console.log('errorchik', error);
      next(new Error('Couldn\'t delete a user'));
    });
});
